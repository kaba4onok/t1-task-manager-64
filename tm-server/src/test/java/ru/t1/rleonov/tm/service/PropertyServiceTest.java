package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.configuration.ServerConfiguration;
import ru.t1.rleonov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @Nullable
    private static IPropertyService PROPERTY_SERVICE;

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
    }

    @Test
    public void getDbSecondLvlCash() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbSecondLvlCash());
    }

    @Test
    public void getDbSchema() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbSchema());
    }

    @Test
    public void getDbFactoryClass() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbFactoryClass());
    }

    @Test
    public void getDbUseQueryCash() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbUseQueryCash());
    }

    @Test
    public void getDbUseMinPuts() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbUseMinPuts());
    }

    @Test
    public void getDbRegionPrefix() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbRegionPrefix());
    }

    @Test
    public void getDbHazelConfig() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbHazelConfig());
    }

    @Test
    public void getDbDialect() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbDialect());
    }

    @Test
    public void getDbDdlAuto() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbDdlAuto());
    }

    @Test
    public void getDbShowSql() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbShowSql());
    }

    @Test
    public void getDbFormatSql() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbFormatSql());
    }

    @Test
    public void getDbDriver() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbDriver());
    }

    @Test
    public void getDbLogin() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbLogin());
    }

    @Test
    public void getDbPassword() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbPassword());
    }

    @Test
    public void getDbUrl() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDbUrl());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerPort());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionTimeout());
    }

}
