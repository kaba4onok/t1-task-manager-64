package ru.t1.rleonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Task;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId);

}
