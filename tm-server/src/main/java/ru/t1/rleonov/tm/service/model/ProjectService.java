package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.model.IProjectService;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.enumerated.UserSort;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.exception.field.StatusEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.repository.model.ProjectRepository;
import ru.t1.rleonov.tm.repository.model.UserRepository;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project>
        implements IProjectService {

    @NotNull
    @Autowired
    public ProjectRepository repository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUser(userRepository.findById(userId).get());
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Project removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        repository.delete(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId,
                                    @Nullable final UserSort userSort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Project> models;
        @Nullable Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (userSort == null) return repository.findAllByUserId(userId, sort);
        switch (userSort) {
            case BY_STATUS:
                sort = Sort.by(Sort.Direction.ASC, "status");
                break;
            case BY_CREATED:
                sort = Sort.by(Sort.Direction.ASC, "created");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "name");
        }
        return repository.findAllByUserId(userId, sort);
    }

}
