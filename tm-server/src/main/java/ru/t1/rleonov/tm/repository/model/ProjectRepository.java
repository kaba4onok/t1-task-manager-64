package ru.t1.rleonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.Project;
import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    void deleteByUserId(@NotNull String userId);

    @Nullable
    Project findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}