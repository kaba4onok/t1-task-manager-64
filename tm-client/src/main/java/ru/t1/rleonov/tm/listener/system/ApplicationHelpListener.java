package ru.t1.rleonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String DESCRIPTION = "Show application commands.";

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener.getName() + " --- " + listener.getDescription());
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
