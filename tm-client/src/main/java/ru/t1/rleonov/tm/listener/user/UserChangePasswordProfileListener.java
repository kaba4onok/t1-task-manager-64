package ru.t1.rleonov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.UserChangePasswordProfileRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-change-password";

    @NotNull
    private static final String DESCRIPTION = "Change password of current user.";

    @Override
    @EventListener(condition = "@userChangePasswordProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordProfileRequest request = new UserChangePasswordProfileRequest(getToken());
        request.setPassword(password);
        getUserEndpoint().changePassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
