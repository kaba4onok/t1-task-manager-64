package ru.t1.rleonov.tm.listener.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.rleonov.tm.listener.AbstractListener;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toDisplayName(task.getStatus()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
