package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataBinaryRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerSaveDataBinaryRequest request = new ServerSaveDataBinaryRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
