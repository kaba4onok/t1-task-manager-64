package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.marker.IntegrationCategory;
import ru.t1.rleonov.tm.service.PropertyService;
import ru.t1.rleonov.tm.util.TestUtil;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;
import static ru.t1.rleonov.tm.util.TestUtil.login;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance("0.0.0.0", "8080");

    @Nullable
    private static String adminToken;

    @Before
    public void setUp() {
        adminToken = login(ADMIN);
    }

    @After
    public void reset() {
        TestUtil.logout(adminToken);
    }

    @Test
    public void loadDataBackup() {
        @NotNull final ServerLoadDataBackupRequest request = new ServerLoadDataBackupRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBackup(request));
    }

    @Test
    public void saveDataBackup() {
        @NotNull final ServerSaveDataBackupRequest request = new ServerSaveDataBackupRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBackup(request));
    }

    @Test
    public void saveDataBase64() {
        @NotNull final ServerSaveDataBase64Request request = new ServerSaveDataBase64Request(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBase64(request));
    }

    @Test
    public void loadDataBase64() {
        @NotNull final ServerLoadDataBase64Request request = new ServerLoadDataBase64Request(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBase64(request));
    }

    @Test
    public void saveDataBinary() {
        @NotNull final ServerSaveDataBinaryRequest request = new ServerSaveDataBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBinary(request));
    }

    @Test
    public void loadDataBinary() {
        @NotNull final ServerLoadDataBinaryRequest request = new ServerLoadDataBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBinary(request));
    }

    @Test
    public void loadDataJsonFasterXml() {
        @NotNull final ServerLoadDataJsonFasterXmlRequest request = new ServerLoadDataJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonFasterXml(request));
    }

    @Test
    public void loadDataJsonJaxB() {
        @NotNull final ServerLoadDataJsonJaxBRequest request = new ServerLoadDataJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonJaxB(request));
    }

    @Test
    public void saveDataJsonFasterXml() {
        @NotNull final ServerSaveDataJsonFasterXmlRequest request = new ServerSaveDataJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonFasterXml(request));
    }

    @Test
    public void saveDataJsonJaxB() {
        @NotNull final ServerSaveDataJsonJaxBRequest request = new ServerSaveDataJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonJaxB(request));
    }

    @Test
    public void loadDataXmlFasterXml() {
        @NotNull final ServerLoadDataXmlFasterXmlRequest request = new ServerLoadDataXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlFasterXml(request));
    }

    @Test
    public void loadDataXmlJaxB() {
        @NotNull final ServerLoadDataXmlJaxBRequest request = new ServerLoadDataXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXmlJaxB(request));
    }

    @Test
    public void saveDataXmlFasterXml() {
        @NotNull final ServerSaveDataXmlFasterXmlRequest request = new ServerSaveDataXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlFasterXml(request));
    }

    @Test
    public void saveDataXmlJaxB() {
        @NotNull final ServerSaveDataXmlJaxBRequest request = new ServerSaveDataXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXmlJaxB(request));
    }

    @Test
    public void loadDataYamlFasterXml() {
        @NotNull final ServerLoadDataYamlFasterXmlRequest request = new ServerLoadDataYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataYamlFasterXml(request));
    }

    @Test
    public void saveDataYamlFasterXml() {
        @NotNull final ServerSaveDataYamlFasterXmlRequest request = new ServerSaveDataYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataYamlFasterXml(request));
    }

}
