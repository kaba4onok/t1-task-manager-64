package ru.t1.rleonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.rleonov.tm.repository.ProjectWebRepository;
import ru.t1.rleonov.tm.repository.TaskWebRepository;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private TaskWebRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectWebRepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}
